package model.language;

import java.util.Locale;
import javax.swing.ImageIcon;

/**
 *Describes the possible languages into which the program can be translated.
 *
 */
public enum Languages {

     /**
     *Describes language English.
     */
     ENGLISH(Locale.US),
    /**
     *Describes language Italian.
     */
     ITALIAN(Locale.ITALY),
    /**
     *Describes language Spanish.
     */
     ESPANOL(new Locale("es", "ES")); 

    private ImageIcon icon;
    private Locale loc;

     /**
      * Constructor that initializes an enum by assigning it a Locale passed by argument and an icon.
      * @param loc Locale
      */
     Languages(final Locale loc) { 
         this.loc = loc;
         this.icon = new ImageIcon(ClassLoader.getSystemResource("image/language.flag." + this.name() + ".png"));
     }

     /**
      * Method that returns the icon of the corresponding enum.
      * @return icon
      */
     public ImageIcon getIcon() {
         return icon;
     }

     /**
      * Method that returns the room to set a bundle, of the corresponding enum.
      * @return Locale
      */
     public Locale getLocale() {
         return loc;
     }
}
