package model.gravity;


import static org.junit.Assert.assertEquals;

import model.entity.cell.cellDead.CellDead;
import model.entity.cell.cellDead.CellDeadImpl;
import model.entity.stone.Stone;
import model.world.World;
import model.world.WorldImp;

/**
 * 
 * test for Gravity class.
 * use of magic numbers
 *
 */
public class TestGravity {

    @org.junit.Test
    public void testGravity() {
        World world = new WorldImp(10, 30);
        Gravity gravity = new GravityImp(world);
        CellDead cell = new CellDeadImpl(5, 0);
        world.putCell(5, 0, cell);
        gravity.cellFallingDown(cell);
        assertEquals(1, cell.getY());
        gravity.cellFallingDown(cell);
        assertEquals(2, cell.getY());
        assertEquals(5, cell.getX());
        world.putCell(3, 5, new Stone(3, 5));
        gravity.cellFallingDown(cell);
        assertEquals(2, cell.getY());
    }

}
