package model.entity;

/**
 * enum defining the type of entity present in the square. At the moment only the cell entity is taken
 *
 */
public enum EntityTypeNameEnum {

    /**
     * cell, in turn it can be of different types.
     */
    CELL,
    /**
     * a stone.
     * a immortal and innamovible object
     */
    STONE,
}
