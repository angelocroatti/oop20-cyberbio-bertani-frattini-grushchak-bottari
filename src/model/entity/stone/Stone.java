package model.entity.stone;

import model.entity.AbstractEntity;
import model.entity.Entity;
import model.entity.EntityTypeNameEnum;

/**
 * 
 * A simple and useless stone.
 *
 */
public class Stone extends AbstractEntity implements Entity {

    /**
     * standard constructor to place the stone in the world.
     * @param x coordinate.
     * @param y coordinate.
     */
    public Stone(final int x, final int y) {
        super(x, y);
    }

    @Override
    public final EntityTypeNameEnum getEntityType() {
        return EntityTypeNameEnum.STONE;
    }
}
