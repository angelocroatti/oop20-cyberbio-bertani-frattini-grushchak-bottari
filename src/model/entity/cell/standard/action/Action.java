package model.entity.cell.standard.action;
/**
 * 
 * interface to perform action in the cell public space.
 */
public interface Action {
    /**
     * 
     * @return
     * the class of action.
     */
    ActionsManipulation getAction();
}
