package model.entity.cell.cellDead;

import model.entity.cell.AbstractCell;
import model.entity.cell.CellTypeNameEnum;
/**
 * 
 * a easy class to create dead cells.
 *
 */
public class CellDeadImpl extends AbstractCell implements CellDead {
    /**
     * a classic constructor.
     * @param x
     * the x.
     * @param y
     * the y.
     */
    public CellDeadImpl(final int x, final int y) {
        super(x, y);
    }

    @Override
    public final CellTypeNameEnum getCellTypeName() {
        return CellTypeNameEnum.CELL_DEAD;
    }

    @Override
    public final void setX(final int x) {
        super.setX(x);
    }

    @Override
    public final void setY(final int y) {
        super.setY(y);
    }

}
