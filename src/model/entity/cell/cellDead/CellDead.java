package model.entity.cell.cellDead;

import model.entity.cell.Cell;
/**
 * 
 * a empty interface for future implementation in deadcell.
 *
 */
public interface CellDead extends Cell {

}
