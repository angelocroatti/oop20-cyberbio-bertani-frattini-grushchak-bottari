package model.entity.cell;

/**
 * enum to declare the type of the cell.
 * 
 *
 */
public enum CellTypeNameEnum {
    /**
     * the standard (and only) cell of the cell type.
     */
    CELL_STANDARD_ALIVE,
    /**
     * the dead cell with no active method.
     */
    CELL_DEAD
}
