package model.world;


import static org.junit.Assert.assertEquals;

import java.util.Optional;

import model.entity.EntityTypeNameEnum;
import model.entity.cell.CellTypeNameEnum;
import model.entity.cell.cellDead.CellDeadImpl;
/**
 * 
 * test for World package.
 * 
 */
public class TestWorld {

    @org.junit.Test
    void testWorld() {

        World world = new WorldImp(75, 60);
        int x = 25;
        int y = 59;
        assertEquals(EntityTypeNameEnum.STONE, world.getType(x, y));
        y = 25;
        assertEquals(Optional.empty(), world.getSquare(x, y).getEntity());
        x *= 2;
        y *= 2;
        world.putCell(x, y, new CellDeadImpl(x, y));
        assertEquals(CellTypeNameEnum.CELL_DEAD, world.getCellType(x, y));
        assertEquals(1, world.getDeadCells());
        assertEquals(0, world.getAliveCells());
        assertEquals(75, world.getWorldWidth());
        assertEquals(60, world.getWorldHeight());
        assertEquals(EntityTypeNameEnum.CELL, world.getType(x, y));
    }
}
