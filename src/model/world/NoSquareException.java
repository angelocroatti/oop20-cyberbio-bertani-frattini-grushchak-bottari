package model.world;

/**
 * 
 * exception launched when you try to access a non-existent square.
 */
public class NoSquareException extends RuntimeException {

    /**
     * automatically generated.
     */
    private static final long serialVersionUID = 1L;

    /**
     * construct a new NoSquareException with a given error message.
     * 
     * @param message
     *                  the error message
     */
    public NoSquareException(final String message) {
        super(message);
    }

}
