package model.genome.decryptor;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import model.genome.factory.GenesEnum;
import model.genome.factory.GenesManager;
import model.genome.genes.Gene;

/**
 * 
 * GeneDecryptor offers access to the gene. Allow get the genes with the indexes or {@link GenesEnum}'s, 
 *
 */
public class GeneDecryptorImpl implements GeneDecryptor {
    /**
     * A genome map from an index generated random to the correspond gene.
     */
    private final Map<Integer, Gene> genomeMap;
    /**
     * Allows get genes of GenesEnum's.
     */
    private final GenesManager genesManager;

    /**
     * @param numberOfGenes the number of possible genes.
     * @param genesManager that can give access to genes.
     */
    public GeneDecryptorImpl(final int numberOfGenes, final GenesManager genesManager) {
        this.genesManager = genesManager;
        this.genomeMap = new InitializeGenomeMap(numberOfGenes).createMap();
    }

    @Override
    public final Gene getGeneOfIndex(final int index) {
        check(!isGenePresent(index));
        return this.genomeMap.get(index);
    }

    @Override
    public final boolean isGenePresent(final int index) {
        return this.genomeMap.containsKey(index);
    }

    // this method is useful for creating a first cell.
    @Override
    public final int getIndexOfGene(final GenesEnum geneEnum) {
        final Optional<Integer> index = genomeMap.entrySet()
                                        .stream()
                                        .filter(f -> f.getValue().equals(getGeneOfEnum(geneEnum)))
                                        .map(m -> m.getKey())
                                        .findFirst();
        check(index.isEmpty());
        return index.get();
    }

    @Override
    public final Gene getGeneOfEnum(final GenesEnum geneEnum) {
        return this.genesManager.getGene(geneEnum);
    }

    private void check(final boolean exception) {
        if (exception) {
            throw new IllegalArgumentException("Invalid argument");
        }
    }

    /**
     * 
     * A class for a genome map creation and initialization.
     *
     */
   private final class InitializeGenomeMap {
        // The list with all possible indexes
        private final List<Integer> indexes;
        private final Random random = new Random();

        /**
         * @param numberOfGenes the number of possible genes.
         */
        private InitializeGenomeMap(final int numberOfGenes) {
            this.indexes = Stream.iterate(0, i -> i + 1)
                            .limit(numberOfGenes)
                            .collect(Collectors.toList());
        }

        /**
         * The method for creating new genome map.
         * 
         * @return genome map.
         */
        public Map<Integer, Gene> createMap() {
            // make the list with all possible genes enums.
            final List<GenesEnum> enums = new LinkedList<>(Arrays.asList(GenesEnum.values()));

            //add some copies of gene enums. So some genes will be appear more frequently.
            for (int i = 0; i < 2; i++) {
                enums.add(GenesEnum.LOOK_AHEAD);
                enums.add(GenesEnum.TURN_FROM_CURRENT_DIRECTION);
                enums.add(GenesEnum.MOVEMENT);
            }

            return genesManager.getGenesList(enums)
                            .stream()
                            .collect(Collectors.toMap(l -> getRandomIndex(), g -> g));
        }

        /**
         * Extract casual index.
         * 
         * @return index for a gene.
         * @throws IllegalStateException if is requested more indexes than the number of genes defined in the constructor.
         */
        private Integer getRandomIndex() {
            if (indexes.isEmpty()) {
                throw new IllegalStateException("There are not free indexes for genes");
            }
            return indexes.remove(random.nextInt(indexes.size()));
        }

    }

}
