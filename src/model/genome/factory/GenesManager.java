package model.genome.factory;

import java.util.List;
import java.util.stream.Collectors;

import model.genome.genes.Gene;

/**
 * 
 * A interface for handle created genes, and offer access to them.
 *
 */
public interface GenesManager {

    /**
     * @param geneEnum one {@link GenesEnum} that represent a gene.
     * @return the gene that is associated with enum given in input.
     * @throws IllegalArgumentException if enum of input is not associated with a gene.
     */
    Gene getGene(GenesEnum geneEnum);

    /**
     * @param geneEnumList a list of {@link GenesEnum}.
     * @return the list with {@link Gene}s that are associated with GenesEnums.
     */
    default List<Gene> getGenesList(final List<GenesEnum> geneEnumList) {
        return geneEnumList.stream().map(this::getGene)
                                    .collect(Collectors.toList());
    }
}
