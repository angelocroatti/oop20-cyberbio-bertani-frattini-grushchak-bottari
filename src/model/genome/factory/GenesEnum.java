package model.genome.factory;

/**
 * 
 * Each enum represent one gene.
 *
 */
public enum GenesEnum {
    /**
     * Reproduction.
     */
    REPRODUCTION,
    /**
     * Mutation.
     */
    MUTATION,
    /**
     * Photosynthesis.
     */
    PHOTOSYNTHESIS,
    /**
     * Minerals absorption.
     */
    MINERALS_ABSORPTION,
    /**
     * Digest minerals.
     */
    DIGEST_MINERALS,
    /**
     * Movement.
     */
    MOVEMENT,
    /**
     * Change direction.
     */
    CHANGE_DIRECTION,
    /**
     * Horizontally aligning.
     */
    HORIZONTALLY_ALIGNING,
    /**
     * Eat all.
     */
    EAT_ALL,
    /**
     * Eat all except relatives.
     */
    EAT_ALL_EXCEPT_RELATIVES,
    /**
     * Look ahead.
     */
    LOOK_AHEAD,
    /**
     * Control space around.
     */
    CONTROL_SPACE_AROUND,
    /**
     * Share energy.
     */
    SHARE_ENERGY,
    /**
     * Share minerals.
     */
    SHARE_MINERALS,
    /**
     * Give resources.
     */
    GIVE_RESOURCES,
    /**
     * Turn from current direction.
     */
    TURN_FROM_CURRENT_DIRECTION,
    /**
     * Death.
     */
    DEATH;
}
