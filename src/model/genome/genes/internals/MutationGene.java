package model.genome.genes.internals;

import java.util.Random;
import model.entity.cell.standard.CellStandard;
import model.genome.genes.Gene;


/**
 * 
 * Mutation gene in some cases changes one gene in genome of cell or changes size of genome.
 *
 */
public class MutationGene implements Gene {
    private static final float PROBABILITY_GENOME_SIZE_CHANGE = 0.2f;
    private final Random rand = new Random();
    private final float mutatioinRate;

    /**
     * @param mutationRate a mutation rate during reproduction.
     */
    public MutationGene(final float mutationRate) {
        super();
        this.mutatioinRate = mutationRate;
    }

    @Override
    public final void launch(final CellStandard cell) {
        if (Math.random() < this.mutatioinRate) {
            if (Math.random() < PROBABILITY_GENOME_SIZE_CHANGE) {
                //with the big genome will be big change.
                final int numNewGenes = rand.nextInt((int) Math.round(Math.sqrt((float) cell.getGenome().size()))) + 1;
                try {
                    cell.changeSideLength(Math.random() < 0.5F ? numNewGenes : -numNewGenes);
                } catch (IndexOutOfBoundsException e) {
                    mutate(cell);
                }
            } else {
               mutate(cell);
            }
        }
    }

    private void mutate(final CellStandard cell) {
        final int position = rand.nextInt(cell.getNumberOfGenes());
        final int newGeneIndex = rand.nextInt(cell.getGenome().size());
        cell.mutateGene(position, newGeneIndex);
    }

    @Override
    public final String getDescription() {
        return "Mutation";
    }

}
