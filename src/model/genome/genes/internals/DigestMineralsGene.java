package model.genome.genes.internals;



import model.entity.cell.standard.CellStandard;
import model.entity.cell.standard.obtainable.EnergyTypeEnum;
import model.genome.genes.Gene;


/**
 * 
 * This class transforms minerals of a cell to energy. 
 *
 */
public class DigestMineralsGene implements Gene {
    private static final int MINERALS_TO_DIGEST = 100;

    @Override
    public final  void launch(final CellStandard cell) {
        if (cell.getMineral() > MINERALS_TO_DIGEST) {
            cell.decrementMineral(MINERALS_TO_DIGEST);
            cell.incrementEnergy(MINERALS_TO_DIGEST, EnergyTypeEnum.CONVERTING_MINERAL);
        } else {
            cell.incrementEnergy(cell.getMineral(), EnergyTypeEnum.CONVERTING_MINERAL);
            cell.decrementMineral(cell.getMineral());
        }
    }

    @Override
    public final String getDescription() {
        return "Digest minerals";
    }

}
