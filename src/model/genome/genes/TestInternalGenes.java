package model.genome.genes;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import model.direction.DirectionEnum;
import model.entity.cell.standard.CellStandard;
import model.entity.cell.standard.prefabCell.CellCreator;
import model.entity.cell.standard.prefabCell.CellCreatorImpl;
import model.genome.decryptor.GeneDecryptor;
import model.genome.decryptor.GeneDecryptorImpl;
import model.genome.factory.GenesEnum;
import model.genome.factory.GenesFactoryImpl;
import model.genome.factory.GenesManagerImpl;
import model.properties.cells.CellDataBuilderImpl;
import model.properties.defaultdata.CellsDefaultUtils;
import model.properties.genes.GenesDataBuilderImpl;
import model.world.WorldImp;

/**
 * 
 * Simple tests for genes.
 *
 */
public class TestInternalGenes {
    private CellCreator creator;
    private GeneDecryptor decryptor;

    /**
     * initialization.
     */
    @BeforeEach
    public void init() {
        this.decryptor = new GeneDecryptorImpl(
                CellsDefaultUtils.NUMBER_OF_GENES.getDafaultValue(),
                new GenesManagerImpl(
                        new GenesFactoryImpl(
                                new GenesDataBuilderImpl(new WorldImp(10, 10)).build())));

        this.creator = new CellCreatorImpl(new CellDataBuilderImpl(this.decryptor).build());
    }

    /**
     * Test genes that change direction of a cell.
     */
    @Test
    public void testChangeDirectionGenes() {
        final CellStandard cell = this.creator.newAllPhotosyntesisCell(0, 0);
        cell.setDirection(DirectionEnum.NORTH);
        cell.setGene(1, DirectionEnum.EAST.getIndex());
        cell.setGene(1, 0);

        assertFalse(cell.getDirection().equals(DirectionEnum.EAST), "Cell direction must be not EAST");
        this.decryptor.getGeneOfEnum(GenesEnum.CHANGE_DIRECTION).launch(cell);
        assertTrue(cell.getDirection().equals(DirectionEnum.EAST), "Direction was changed to EAST");

        this.decryptor.getGeneOfEnum(GenesEnum.TURN_FROM_CURRENT_DIRECTION).launch(cell);
        assertFalse(cell.getDirection().equals(DirectionEnum.EAST), "direction was changed");
        assertTrue(cell.getDirection().equals(DirectionEnum.NORTHEAST) 
                || cell.getDirection().equals(DirectionEnum.SOUTHEAST), "Cell turn from its direction");

        cell.setDirection(DirectionEnum.NORTH);
        this.decryptor.getGeneOfEnum(GenesEnum.HORIZONTALLY_ALIGNING).launch(cell);
        assertTrue(cell.getDirection() == DirectionEnum.EAST 
                || cell.getDirection() == DirectionEnum.WEST, "Direction muts me to east or west");
    }

    /**
     * Test genes that use minerals.
     */
    @Test
    public void testMineralsGenes() {
        final CellStandard cell = this.creator.newAllPhotosyntesisCell(0, 9);
        cell.decrementEnergy(100);
        final int energy = cell.getEnergy();

        assertEquals(cell.getMineral(), 0, "cell has not minerals");
        this.decryptor.getGeneOfEnum(GenesEnum.MINERALS_ABSORPTION).launch(cell);
        assertTrue(cell.getMineral() > 0, "cell absorbed minerals");

        assertEquals(cell.getEnergy(), energy, "cell energy was not changed");
        this.decryptor.getGeneOfEnum(GenesEnum.DIGEST_MINERALS).launch(cell);
        assertTrue(cell.getEnergy() > energy, "Cell digest minerals and receive energy");
        assertEquals(cell.getMineral(), 0, "cell has spend its minerals");
    }

    /**
     * Test photosynthesis gene.
     */
    @Test
    public void testPhotosynthesisGene() {
        final CellStandard cell = this.creator.newAllPhotosyntesisCell(0, 0);
        cell.decrementEnergy(100);
        final int energy = cell.getEnergy();

        this.decryptor.getGeneOfEnum(GenesEnum.PHOTOSYNTHESIS).launch(cell);
        assertTrue(cell.getEnergy() > energy, "cell received energy from photosynthesis");

        final int diffA = cell.getEnergy() - energy;
        cell.decrementEnergy(diffA);
        cell.setY(3);

        assertEquals(energy, cell.getEnergy(), "cell energy reseted");
        this.decryptor.getGeneOfEnum(GenesEnum.PHOTOSYNTHESIS).launch(cell);
        assertTrue(cell.getEnergy() > energy, "cell received energy from photosynthesis");

        final int diffB = cell.getEnergy() - energy;
        assertTrue(diffA > diffB, "in first place cell received more energy");
    }

}
