package model.genome.genes.externals;


import model.entity.cell.standard.CellStandard;
import model.entity.cell.standard.obtainable.EnergyTypeEnum;
import model.world.World;

/**
 * 
 * If in direction of the cell is an another cell with less
 * energy, the cell share energy with another for make their energies equal.
 *
 */
public class ShareEnergyGene extends GiveResourcesGene {
    /**
     * @param world       the current world of simulation.
     */
    public ShareEnergyGene(final World world) {
        super(world);
    }

    /**
     * Make a cell share energy with an another cell.
     * @param cell that shares energy.
     * @param receiverCell cell that receives energy.
     */
    protected void share(final CellStandard cell, final CellStandard receiverCell) {
        if (cell.getEnergy() > receiverCell.getEnergy()) {
            final int energy = (cell.getEnergy() - receiverCell.getEnergy()) / 2;
            cell.decrementEnergy(energy); 
            receiverCell.incrementEnergy(energy, EnergyTypeEnum.ALTRUISM);
        }
    }

    @Override
    public final String getDescription() {
        return "Share energy";
    }
}
