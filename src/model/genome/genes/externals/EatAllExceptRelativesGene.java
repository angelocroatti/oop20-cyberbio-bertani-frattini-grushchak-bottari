package model.genome.genes.externals;

import model.entity.cell.standard.CellStandard;
import model.world.World;

/**
 * 
 * A cell try to eat another cell in an direction if it is not relatives of cell.
 *
 */
public class EatAllExceptRelativesGene extends EatAllGene implements CellFamily {

    /**
     * @param world the current world of simulation.
     * @param nutritionOfDeadCell the quantity of energy that receive cell if it ate an dead cell.
     */
    public EatAllExceptRelativesGene(final World world, final int nutritionOfDeadCell) {
        super(world, nutritionOfDeadCell);
    }

    /**
     * Make cell to try eat the victim. But if victim is a relatives of cell, cell does not attack.
     * @param cell that want eats  another cell.
     * @param victim the cell that will be attacked.
     */
    @Override
    protected final void eatStandardCell(final CellStandard cell, final CellStandard victim) {
        if (!areRelatives(cell, victim)) {
            battle(cell, victim);
        }
    }

    @Override
    public final String getDescription() {
        return "Attack all except relatives";
    }
}
