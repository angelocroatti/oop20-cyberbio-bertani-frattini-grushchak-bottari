package model.genome.genes.externals;

import java.util.Optional;

import model.entity.Entity;
import model.entity.EntityTypeNameEnum;
import model.entity.cell.CellTypeNameEnum;
import model.entity.cell.standard.CellStandard;
import model.entity.cell.standard.obtainable.EnergyTypeEnum;
import model.world.World;

/**
 * 
 * If in direction of the cell is an another cell, the cell give mineral to the
 * another cell.
 * 
 */
public class GiveResourcesGene extends AbstractExternalGene {
    /**
     * @param world       the current world of simulation.
     */
    public GiveResourcesGene(final World world) {
        super(world);
    }

    @Override
    public final void launch(final CellStandard cell) {
        final Optional<Entity> something = getEntityInDirection(cell.getX(), cell.getY(), cell.getDirection());

        if (something.isPresent() && something.get().getEntityType().equals(EntityTypeNameEnum.CELL)) {
            @SuppressWarnings ("unchecked")
            final Optional<CellStandard> receiverCell = (Optional<CellStandard>) tryCastToSpecificCell(something.get(), CellTypeNameEnum.CELL_STANDARD_ALIVE);

            if (receiverCell.isPresent()) {
                share(cell, receiverCell.get());
            }
        }
    }

    /**
     * Make cell gives 1/4 of energy and minerals to an another cell.
     * @param cell a cell that give resources.
     * @param receiverCell a cell that receive resources.
     */
    protected void share(final CellStandard cell, final CellStandard receiverCell) {
        final int minerals = cell.getMineral() / 4;
        cell.decrementMineral(minerals);
        receiverCell.incrementMineral(minerals);

        final int energy = cell.getEnergy() / 4;
        cell.decrementEnergy(energy);
        receiverCell.incrementEnergy(energy, EnergyTypeEnum.ALTRUISM);
    }

    /**
     * The class that extends this must override getDesctiption method.
     */
    @Override
    public String getDescription() {
        return "Give resources";
    }

}
