package model.genome.genes.steps;

/**
 * 
 * Enumeration that hold relative step to the next gene from the current gene index of a cell.
 *
 */
public enum GenomeStepEnum implements GenomeStep {

    /**
     * 
     */
    ONE, TWO, THREE, FOUR, FIVE;

    @Override
    public final int getStep() {
        return this.ordinal() + 1;
    }

}
