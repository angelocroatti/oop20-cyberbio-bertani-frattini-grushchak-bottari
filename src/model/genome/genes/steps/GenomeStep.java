package model.genome.genes.steps;

/**
 * 
 * The interface for hold relative distance from current genome index of
 * cell to the next genome index.
 *
 */
public interface GenomeStep {

    /**
     * @return the relative distance to next genome index.
     */
    int getStep();
}
