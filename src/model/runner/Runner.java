package model.runner;

/**
 * interface for class that runs cells.
 *
 */
public interface Runner {
    /**
     * runs operations for all the alive cells in the world simulating a day.
     * 
     */
    void aliveCells();

    /**
     *runs operations for the dead cells in the world simulating a day.
     *
     */
    void deadCells();

}
