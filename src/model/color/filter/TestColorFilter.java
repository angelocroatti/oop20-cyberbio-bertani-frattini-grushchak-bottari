package model.color.filter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import controller.StartingMode;
import controller.file.data.input.FileSet;
import controller.file.data.input.FileSetImpl;
import model.entity.cell.cellDead.CellDeadImpl;
import model.entity.stone.Stone;
import model.square.Square;
import model.square.SquareImp;

/**
 * Tests to test the colors returned by the filters for the main entities.
 *
 */
class TestColorFilter {
    private final Filter ageFilter = new AgeFilter();
    private final Filter energyFilter = new EnergyFilter();
    private final Filter nutritionFilter = new NutritionFilter();

    /**
     * initialization.
     */
    @BeforeEach
    public void init() {
       final FileSet f = new FileSetImpl();
       f.addtoFile(100, 100, 1, 100, 1, 1, 1, 0, StartingMode.SINGLE_PHOTOSYNTHESIS_CELL, 0, 0, 0);
    }

    /**
     * Test whit SQUARE EMPTY.
     */
    @Test
    public void testEmpty() {
        final Square squareEmpty = new SquareImp(Optional.empty());
        assertEquals(ColorProgram.BACKGROUND_COLOR.getColor(), ageFilter.getColor(squareEmpty.getEntity()), "The color should be BACKGROUND_COLOR");
        assertEquals(ColorProgram.BACKGROUND_COLOR.getColor(), energyFilter.getColor(squareEmpty.getEntity()), "The color should be BACKGROUND_COLOR");
        assertEquals(ColorProgram.BACKGROUND_COLOR.getColor(), nutritionFilter.getColor(squareEmpty.getEntity()), "The color should be BACKGROUND_COLOR");
    }

    /**
     * Test whit STONE.
     */
    @Test
    public void testStone() {
        final Square squareStone = new SquareImp(Optional.of(new Stone(0, 0)));
        assertEquals(ColorProgram.STONE_COLOR.getColor(), ageFilter.getColor(squareStone.getEntity()), "The color should be STONE_COLOR");
        assertEquals(ColorProgram.STONE_COLOR.getColor(), energyFilter.getColor(squareStone.getEntity()), "The color should be STONE_COLOR");
        assertEquals(ColorProgram.STONE_COLOR.getColor(), nutritionFilter.getColor(squareStone.getEntity()), "The color should be STONE_COLOR");
    }

    /**
     * Test whit CELL DEAD.
     */
    @Test
    public void testCellDead() {
        final Square squareCellDead = new SquareImp(Optional.of(new CellDeadImpl(0, 0)));
        assertEquals(ColorProgram.CELL_DEATH_COLOR.getColor(), ageFilter.getColor(squareCellDead.getEntity()), "The color should be CELL_DEATH_COLOR");
        assertEquals(ColorProgram.CELL_DEATH_COLOR.getColor(), energyFilter.getColor(squareCellDead.getEntity()), "The color should be CELL_DEATH_COLOR");
        assertEquals(ColorProgram.CELL_DEATH_COLOR.getColor(), nutritionFilter.getColor(squareCellDead.getEntity()), "The color should be CELL_DEATH_COLOR");
    }
}
