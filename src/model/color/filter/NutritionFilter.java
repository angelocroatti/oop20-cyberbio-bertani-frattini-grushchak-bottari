package model.color.filter;

import java.awt.Color;

import model.entity.cell.standard.CellStandard;
import model.entity.cell.standard.obtainable.EnergyTypeEnum;
import model.properties.defaultdata.ViewDefaultUtils;

/**
 * Class that manages the color of standard cells according to their nutrition. The color of the cell takes on different 
 * shades depending on the type of nourishment used by the cells to accumulate energy during their lifetime.
 * 
 */
public class NutritionFilter extends FilterImpl {

    private static final int MAX_RANGE_COLOR = ViewDefaultUtils.COLOR_RGB_RANGE.getMaximumValue();
    private int r;
    private int g;
    private int b;

    /**
     * Calculates the color of the past STANDARD_CELL as a parameter based on the nutrition during the life of the cell.
     * Depending on the percentage of nourishment that the cell has used during its lifetime, it will take on tending colors:
     * RED COLORS-> NOURISHMENT FROM OTHER CELLS
     * GREEN COLORS-> PHOTOSYNTHETIC NOURISHMENT
     * BLUE COLORS-> MINERAL NOURISHMENT
     * @param cellstandard cell whose color you want to know the color
     * @return cell color 
     * 
     */
    @Override
    protected final Color getCellStandardColor(final CellStandard cellstandard) {
        final int energyTot = cellstandard.getTotalEnergyGained();
        if (energyTot == 0) {
            return new Color(MAX_RANGE_COLOR, MAX_RANGE_COLOR, MAX_RANGE_COLOR);
        } else {
            setValue(cellstandard);
            return new Color(getValue(r, energyTot), getValue(g, energyTot), getValue(b, energyTot));
        }
    }

    private void setValue(final CellStandard cellstandard) {
        r = cellstandard.getSpecificEnergyGained(EnergyTypeEnum.ALTRUISM) + cellstandard.getSpecificEnergyGained(EnergyTypeEnum.EATING);
        g = cellstandard.getSpecificEnergyGained(EnergyTypeEnum.PHOTOSYNTHESIS);
        b = cellstandard.getSpecificEnergyGained(EnergyTypeEnum.CONVERTING_MINERAL);
    }

    private int getValue(final int val, final int energyTot) {
        return val * MAX_RANGE_COLOR / energyTot;
    }
}
