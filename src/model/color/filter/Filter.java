package model.color.filter;

import java.awt.Color;
import java.util.Optional;

import model.entity.Entity;

/**
 * Interface that manages the colors of the entities belonging to the world according to the type of entities and their 
 * characteristics.
 * 
 */
public interface Filter {

    /**
     * Calculates the color of any entity based on its type and characteristics.
     * STONE-> dark grey
     * DEAD CELL -> light grey
     * CELL STANDARD-> return the color depends on its characteristics
     * @param square the entity of which you want to know the color
     * @return the color of entity
     * @throws IllegalArgumentException if the cell type is not one of the existing types 
     */
    Color getColor(Optional<Entity> square);

}
