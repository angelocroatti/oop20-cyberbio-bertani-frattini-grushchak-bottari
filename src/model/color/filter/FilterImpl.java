package model.color.filter;

import java.awt.Color;
import java.util.Optional;

import model.entity.Entity;
import model.entity.cell.Cell;
import model.entity.cell.standard.CellStandard;

/**
 * Abstract class that manages the colors of entities that are not standard cells and therefore with colors common to all 
 * filters, according to their type.
 *
 */
public abstract class FilterImpl implements Filter {

    @Override
    public final Color getColor(final Optional<Entity> square) {
       Color color;
       if (square.isPresent()) {
           color = colorEntity(square.get());
       } else {
           color = ColorProgram.BACKGROUND_COLOR.getColor();
       }
       return color;

    }

    private Color colorEntity(final Entity entity) {
        Color entityColor;
        switch (entity.getEntityType()) {
            case CELL: entityColor = cellColor((Cell) entity); break;
            case STONE: entityColor = ColorProgram.STONE_COLOR.getColor(); break;
            default: throw new IllegalArgumentException("TIPO DI ENTITY INESISTENTE");
        }
        return entityColor;
    }

    private Color cellColor(final Cell cell) {
        Color cellColor;
        switch (cell.getCellTypeName()) {
            case CELL_STANDARD_ALIVE: 
                cellColor = getCellStandardColor((CellStandard) cell);
                break;
            case CELL_DEAD:
                cellColor = ColorProgram.CELL_DEATH_COLOR.getColor();
                break;
            default: throw new IllegalArgumentException("TIPO DI CELL INESISTENTE");
        }
        return cellColor;
    }

    /**
     * Abstract method that calculates the color of standard cells according to their characteristics.
     * @param cellstandard cell whose color you want to know the color
     * @return cell color
     */
    protected abstract Color getCellStandardColor(CellStandard cellstandard);

}

