package model.properties.cells;

import model.genome.decryptor.GeneDecryptor;
import model.properties.defaultdata.CellsDefaultUtils;
import model.properties.defaultdata.DefaultDataContainer;
import model.properties.utilities.NumbersComparator;

/**
 * 
 * A builder that can build a {@link CellData}. Can create only a single instance.
 *
 */
public class CellDataBuilderImpl implements CellDataBuilder {
    private final GeneDecryptor geneDecryptor;
    private int turnCost;
    private int genomeSize;
    private int maxEnergy;
    private int maxMinerals;
    private int maxAge;
    private int numberOfGenes;

    private boolean isBuild;

    /**
     * @param decryptor that give access to genes.
     */
    public CellDataBuilderImpl(final GeneDecryptor decryptor) {
        this.geneDecryptor = decryptor;

        this.turnCost = CellsDefaultUtils.TURN_COST.getDafaultValue();
        this.genomeSize = CellsDefaultUtils.GENOME_SIZE.getDafaultValue();
        this.maxEnergy = CellsDefaultUtils.MAX_CELL_ENERGY.getDafaultValue();
        this.maxMinerals = CellsDefaultUtils.MAX_CELL_MINERALS.getDafaultValue();
        this.maxAge = CellsDefaultUtils.MAX_AGE.getDafaultValue();
        this.numberOfGenes = CellsDefaultUtils.NUMBER_OF_GENES.getDafaultValue();
    }

    private void controlIsBuilt() {
        if (this.isBuild) {
            throw new IllegalStateException("This builder has already built");
        }
    }

    private void checkLimits(final int value, final DefaultDataContainer<? extends Number> defaultData) {
        check(NumbersComparator.isBiggerThan(defaultData.getMinimumValue(), value)
                || NumbersComparator.isBiggerThan(value, defaultData.getMaximumValue()));
    }

    private void check(final boolean bool) {
        if (bool) {
            throw new IllegalArgumentException("The parameter is out of limit");
        }
    }

    @Override
    public final CellDataBuilder setTurnCost(final int cost) {
        controlIsBuilt();
        checkLimits(cost, CellsDefaultUtils.TURN_COST);
        this.turnCost = cost;
        return this;
    }

    @Override
    public final CellDataBuilder setGenomeSize(final int size) {
        controlIsBuilt();
        checkLimits(size, CellsDefaultUtils.GENOME_SIZE);
        this.genomeSize = size;
        return this;
    }

    @Override
    public final CellDataBuilder setMaxEnergy(final int maxEnergy) {
        controlIsBuilt();
        checkLimits(maxEnergy, CellsDefaultUtils.MAX_CELL_ENERGY);
        this.maxEnergy = maxEnergy;
        return this;
    }

    @Override
    public final CellDataBuilder setMaxMinerals(final int maxMinerals) {
        controlIsBuilt();
        checkLimits(maxMinerals, CellsDefaultUtils.MAX_CELL_MINERALS);
        this.maxMinerals = maxMinerals;
        return this;
    }

    @Override
    public final CellDataBuilder setMaxAge(final int maxAge) {
        controlIsBuilt();
        checkLimits(maxAge, CellsDefaultUtils.MAX_AGE);
        this.maxAge = maxAge;
        return this;
    }

    @Override
    public final CellDataBuilder setNumberOfGenes(final int number) {
        controlIsBuilt();
        checkLimits(number, CellsDefaultUtils.NUMBER_OF_GENES);
        if ((number % CellsDefaultUtils.NUMBER_GENES_MULTIPLE != 0)) {
            throw new IllegalArgumentException("The number of genes must be multple of " + CellsDefaultUtils.NUMBER_GENES_MULTIPLE);
        }
        this.numberOfGenes = number;
        return this;
    }

    @Override
    public final CellData build() {
        controlIsBuilt();
        this.isBuild = true;

        return new CellData() {

            @Override
            public GeneDecryptor getGeneDecryptor() {
                return geneDecryptor;
            }

            @Override
            public int getTurnCost() {
                return turnCost;
            }

            @Override
            public int getGenomeSize() {
                return genomeSize;
            }

            @Override
            public int getMaxEnergy() {
                return maxEnergy;
            }

            @Override
            public int getMaxMinerals() {
                return maxMinerals;
            }

            @Override
            public int getMaxAge() {
                return maxAge;
            }

            @Override
            public int getNumberOfGenes() {
                return numberOfGenes;
            }
        };
    }

}
