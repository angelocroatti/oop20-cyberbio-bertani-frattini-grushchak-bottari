package model.properties.defaultdata;

/**
 * 
 * Every constant contains a container with limits and default value of different utilities utilized by genes.
 *
 */
public final class GenesDefaultUtils {
    /**
     * Contains integer values of the minimum, maximum and default cost of cellular reproduction.
     */
    public static final DefaultDataContainer<Integer> REPRODUCTION_COST = new DefaultDataContainerImpl<Integer>(0, 400, 100);
    /**
     * Contains integer values of the minimum, maximum and default sun energy in simulation.
     */
    public static final DefaultDataContainer<Integer> SUN_ENERGY = new DefaultDataContainerImpl<Integer>(0, 30, 15);
    /**
     * Contains integer values of the minimum, maximum and default number of minerals that can be
     * absorbed by cell in one turn.
     */
    public static final DefaultDataContainer<Integer> MINERALS_ABSORPTION = new DefaultDataContainerImpl<Integer>(1, 10, 5);
    /**
     * Contains integer values of the minimum, maximum and default number of energy that a cell
     * receives if it ate a dead cell.
     */
    public static final DefaultDataContainer<Integer> NUTRITION_OF_DEAD_CELL = new DefaultDataContainerImpl<Integer>(0, 500, 120);
    /**
     * Contains floats values of the minimum, maximum and default sun depth that define the first (100*n)%
     * of lines of world with possibility to photosynthesis.
     */
    public static final DefaultDataContainer<Float> SUN_PENETRATION = new DefaultDataContainerImpl<Float>(0F, 1F, 0.6F);
    /**
     * Contains floats values of the minimum, maximum and default minerals depth that define the last
     * (100*(1-n))% of lines of the world with possibility to receive minerals.
     */
    public static final DefaultDataContainer<Float> MINERALS_DEPTH = new DefaultDataContainerImpl<Float>(0F, 1F, 0.4F);
    /**
     * Contains floats values of the minimum, maximum and float default mutation rate.
     */
    public static final DefaultDataContainer<Float> MUTATION_RATE = new DefaultDataContainerImpl<Float>(0.05F, 1F, 0.2F);

    private GenesDefaultUtils() {

    }
}
