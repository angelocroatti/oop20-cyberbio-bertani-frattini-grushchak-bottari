package model.properties.defaultdata;

/**
 * 
 * A interface for a container for different possible values of data.
 * @param <T> any types that extends {@link Number}.Better to use Integer or Float.
 *
 */
public interface DefaultDataContainer<T extends Number> {
    /**
     * @return a minimum value.
     */
    T getMinimumValue();

    /**
     * @return a maximum value.
     */
    T getMaximumValue();

    /**
     * @return a default value.
     */
    T getDafaultValue();
}
