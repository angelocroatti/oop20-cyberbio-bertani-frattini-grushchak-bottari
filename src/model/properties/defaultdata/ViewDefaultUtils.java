package model.properties.defaultdata;

/**
 * Every constant contains a container with limits and default value of different
 * utilities utilized by view.
 *
 */
public final class ViewDefaultUtils {
    /**
     * Contains integer values of the minimum, maximum and default number that represents after how
     * many cycles the world must be redesigned on screen.
     */
    public static final DefaultDataContainer<Integer> UPDATE_VIEW = new DefaultDataContainerImpl<Integer>(1, 30, 1);
    /**
     * Contains the float values of the minimum, maximum and default number of hue values in the HSB model.
     */
    public static final DefaultDataContainer<Float> COLOR_HSB_RANGE = new DefaultDataContainerImpl<Float>(0F, 360F, 0F);
    /**
     * Contains the integer values of the minimum, maximum and default number of hue values in the HSB model.
     */
    public static final DefaultDataContainer<Integer> COLOR_RGB_RANGE = new DefaultDataContainerImpl<Integer>(0, 255, 0);

    private ViewDefaultUtils() {

    }
}

