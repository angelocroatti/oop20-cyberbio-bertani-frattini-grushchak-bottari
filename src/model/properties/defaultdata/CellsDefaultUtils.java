package model.properties.defaultdata;

/**
 * 
 * Every constant contains a container with limits and default value of different utilities utilized by cells.
 *
 */
public final class CellsDefaultUtils {
    /**
     *  Contains integer values of the minimum, maximum and default number of the maximum cell energy.
     */
    public static final DefaultDataContainer<Integer> MAX_CELL_ENERGY = new DefaultDataContainerImpl<Integer>(250, 2000, 1000);
    /**
     *  Contains integer values of the minimum, maximum and default number of the maximum minerals that can be contained into cell.
     */
    public static final DefaultDataContainer<Integer> MAX_CELL_MINERALS = new DefaultDataContainerImpl<Integer>(100, 5000, 1000);
    /**
     * Contains integer values of the minimum, maximum and default number of the energy that must be spend by cell every turn.
     */
    public static final DefaultDataContainer<Integer> TURN_COST = new DefaultDataContainerImpl<Integer>(0, 3, 1);
    /**
     * Contains integer values of the minimum, maximum and default number of the maximum age of cells.
     */
    public static final DefaultDataContainer<Integer> MAX_AGE = new DefaultDataContainerImpl<Integer>(200, 10000, 3000);
    /**
     * Contains integer values of the minimum, maximum and default number of the initial cell genome size.
     */
    public static final DefaultDataContainer<Integer> GENOME_SIZE = new DefaultDataContainerImpl<Integer>(5, 30, 20);
    /**
     * Contains integer values of the minimum, maximum and default number of the different types of genes.
     */
    public static final DefaultDataContainer<Integer> NUMBER_OF_GENES = new DefaultDataContainerImpl<Integer>(
                                                                    CellsDefaultUtils.NUMBER_GENES_MULTIPLE * 4, 
                                                                    CellsDefaultUtils.NUMBER_GENES_MULTIPLE * 8,
                                                                    CellsDefaultUtils.NUMBER_GENES_MULTIPLE * 5);

    /**
     * The number of types of genes must be multiple of this value.
     */
    public static final int NUMBER_GENES_MULTIPLE = 8;

    private CellsDefaultUtils() {

    }
}
