package model.properties.defaultdata;

/**
 * 
 * Every constant contains a container with limits and default value of different utilities utilized by world.
 *
 */
public final class WorldDefaultUtils  {
    /**
     * Contains integer values of the minimum, maximum and default world height size.
     */
    public static final DefaultDataContainer<Integer> WORLD_HEIGHT = new DefaultDataContainerImpl<Integer>(10, 100, 50);
    /**
     * Contains integer values of the minimum, maximum and default world width size.
     */
    public static final DefaultDataContainer<Integer> WORLD_WIDTH = new DefaultDataContainerImpl<Integer>(10, 130, 70);

    private WorldDefaultUtils() {

    }
}
