package model.properties.genes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import model.world.WorldImp;

/**
 * Test of {@link GenesDataBuilder}.
 */
public class TestGenesData {
    private GenesDataBuilder builder;

    /**
     * Initialization.
     */
    @BeforeEach
    public void init() {
        this.builder = new GenesDataBuilderImpl(new WorldImp(10, 10));
    }

    /**
     * Test builder.
     */
    @Test
    public void genesBuilderTest() {
        final int cost = 200;
        final int absorption = 3;
        final int nutrition = 300;
        final int sun = 15;
        final float mutation = 0.2F;
        final float penetration = 0.5F;
        final float depth = 0.5F;
        final GenesData data = this.builder.setReproductionCost(cost)
                                .setMineralsAbsorption(absorption)
                                .setMineralsDepth(depth)
                                .setMutationRate(mutation)
                                .setNutritionOfDeadCell(nutrition)
                                .setSunEnergy(sun)
                                .setSunPenetration(penetration)
                                .build();

        assertEquals(cost, data.getReproductionCost(), "test reproduntion cost");
        assertEquals(absorption, data.getMineralsAbsorption(), "test minerals absorption");
        assertEquals(depth, data.getMineralsDepth(), "test minerals depth");
        assertEquals(mutation, data.getMutationRate(), "test mutation rate");
        assertEquals(nutrition, data.getNutritionOfDeadCell(), "test nutrition of det dead cell");
        assertEquals(sun, data.getSunEnergy(), "test sun energy");
        assertEquals(penetration, data.getSunPenetration(), "test sun penetrations");

    }

    /**
     * Test for GenesDataBuilder exception.
     */
    @Test
    public void genesBuilderExeptionTest() {
        assertThrows(IllegalArgumentException.class, () -> this.builder.setMineralsAbsorption(-1).build());
        assertThrows(IllegalArgumentException.class, () -> this.builder.setMineralsDepth(-1F).build());
        assertThrows(IllegalArgumentException.class, () -> this.builder.setReproductionCost(-1).build());

        builder.build();
        assertThrows(IllegalStateException.class, () -> {
            this.builder.build();
        });
        assertThrows(IllegalStateException.class, () -> {
            this.builder.setMineralsAbsorption(1);
        });
        assertThrows(IllegalStateException.class, () -> {
            this.builder.setMutationRate(1F);
        });
    }
}
