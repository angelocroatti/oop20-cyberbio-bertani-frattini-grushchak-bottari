package model.caster;

import static org.junit.Assert.assertEquals;

import model.entity.EntityTypeNameEnum;
import model.entity.cell.CellTypeNameEnum;
import model.entity.cell.cellDead.CellDeadImpl;
import model.world.World;
import model.world.WorldImp;

/**
 * 
 * test for CastClass.
 * use of magic numbers
 *
 */
public class TestCaster {

    @org.junit.Test
    void testCaster() {

        World world = new WorldImp(15, 10);
        CastClass cast = new CastClassImp();

        assertEquals(EntityTypeNameEnum.STONE, cast.stoneCast(world.getSquare(0, 9).getEntity().get()).getEntityType());
        world.putCell(5, 5, new CellDeadImpl(5, 5));
        assertEquals(CellTypeNameEnum.CELL_DEAD, cast.cellDeadCast(world.getSquare(5, 5).getEntity().get()).getCellTypeName());
    }

}
