package application;

import view.menu.language.MenuFrame;
import view.menu.language.LanguageMenuImpl;

/**
 * 
 * The main class of application.
 *
 */
final class CyberBiology {

    private final MenuFrame languageMenu;

    private CyberBiology() {
        this.languageMenu = new LanguageMenuImpl();
    }
    private void start() {
        this.languageMenu.showMenu();
    }

    /**
     * @param arg unused.
     */
    public static void main(final String... arg) {
        new CyberBiology().start();
    }
}
