package view.start.world;

import controller.Start;
import controller.StartingMode;
import controller.file.data.input.FileSet;
import controller.file.data.input.FileSetImpl;
import model.language.Language;
import model.language.LanguageSet;
import model.language.Languages;
import model.world.WorldImp;

/**
 * Test class that launches the application graphic interface, without implemented world, to view its structure.
 *
 */
public final class TestGraphicApplication {

    private TestGraphicApplication() { };

    /**
     * Launch the main application window.
     * @param args default
     */
    public static void main(final String[] args) {
        final LanguageSet l = new Language();
        l.setcurrentbundle(Languages.ENGLISH);
        final FileSet setterTest = new FileSetImpl();
        setterTest.addtoFile(10, 10, 10, 10, 10, 10, 10, 1, StartingMode.RANDOM_GENOME_CELLS, (float) 1, (float) 1, (float) 1);

        final Thread t = new Thread(new Start(new WorldImp(10, 10)));
        t.start();
    }
}
