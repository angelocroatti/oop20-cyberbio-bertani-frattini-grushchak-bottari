package view.start.world;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.world.CellInfosImp;
import model.world.World;
import view.clock.ViewClock;
import view.menu.data.setting.AddElem;
import view.menu.data.setting.AddElemValue;
import view.menu.data.setting.Icon;
import view.menu.language.MenuFrame;

/**
 * Class that implements the main window of the program with the specifications related to the world, the clock, button of stop,
 * the world and the ability to reset the language and filter for updating the world.
 */
public class ApplicationFrameImpl implements ApplicationFrame, MenuFrame {

    private final AddElemValue<Boolean> stop = stopButton();
    private final AddElem world;
    private final AddElem specific;
    private final AddElemValue<JLabel> clockPanel = new ViewClock();
    private final JFrame startFrame = new JFrame();

    /**
     * Builder that creates a graphical window by entering the world view from the one passed by parameter and adding 
     * the clock, the specifications always referred to wordImpl and the menu for setting the filters and language.
     * @param worldImpl the implemented world containing the elements you want to display
     */
    public ApplicationFrameImpl(final World worldImpl) {
        super();
        this.world = new ViewWorldImpl(worldImpl);
        this.specific = new SpecificWorld(new CellInfosImp(worldImpl));
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(world.getElem());
        panel.add(otherPanel());
        final AddElem menuUpdate = new ChangeDate((ViewWorldImpl) world, (SpecificWorld) specific, (ViewClock) clockPanel, stop);
        panel.add(menuUpdate.getElem());
        startFrame.add(panel);
        startFrame.setIconImage(Icon.APPLICATION.getIcon().getImage());
        startFrame.setTitle("GENOMY");
        startFrame.setResizable(false);
        startFrame.pack();
        startFrame.setLocationRelativeTo(null);
        startFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                ((JButton) stop.getElem()).doClick();
                System.exit(0);
            }
        });
    }

    @Override
    public final void showMenu() {
        startFrame.setVisible(true);
    }

    @Override
    public final UpdateWorld getWorld() {
        return (UpdateWorld) world;
    }

    @Override
    public final AddElemValue<Boolean> getButton() {
        return stop;
    }

    @Override
    public final JLabel getClockView() {
        return clockPanel.getValue();
    }

    @Override
    public final UpdateSpecific getSpecific() {
        return (UpdateSpecific) specific;
    }

    private JPanel otherPanel() {
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(clockPanel.getElem());
        panel.add(specific.getElem());
        final JPanel stopPanel = new JPanel();
        stopPanel.add(stop.getElem());
        panel.add(stopPanel);
        return panel;
    }

    private AddElemValue<Boolean> stopButton() {
        final JButton jb = new JButton("STOP");
        jb.addActionListener(a -> {
            jb.setEnabled(false);
            jb.setBackground(Color.LIGHT_GRAY);
        });
        return new AddElemValue<Boolean>() {
            @Override
            public JButton getElem() {
                jb.setAlignmentX(Component.CENTER_ALIGNMENT);
                jb.setAlignmentX(Component.CENTER_ALIGNMENT);
                return jb;
            }

            @Override
            public Boolean getValue() {
                return jb.isEnabled();
            }
        };
    }
}
