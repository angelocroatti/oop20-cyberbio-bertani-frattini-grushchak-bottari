package view.start.world;

import java.awt.Component;
import java.awt.GridLayout;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.color.filter.Filters;
import model.language.Language;
import model.language.LanguageSet;
import model.language.Languages;
import view.clock.ViewClock;
import view.menu.data.setting.AddElem;
import view.menu.data.setting.AddElemValue;
import view.menu.data.setting.DimensionComponent;
import view.menu.language.ButtonLanguage;

/**
 * Class that implements a graphical menu to change the language and color filter when running the application.
 */
public class ChangeDate extends JPanel implements AddElem {

    private static final long serialVersionUID = -6659193328896165020L;
    private final JLabel changeLangDesc = new JLabel(Language.getkeyofbundle("ChangeL"));
    private final JLabel changeFiltDesc = new JLabel(Language.getkeyofbundle("ChangeF"));
    private final Map<JButton, Filters> filters = new HashMap<>();
    private final JButton jb = new JButton(Language.getkeyofbundle("Confirm"));

    /**
     * Builder that adds menus to a panel that allows you to update the values used as parameters.
     * @param world graphic interface of the world 
     * @param specific interface of the specific's world
     * @param clockPanel graphic interface of the clock
     * @param stop button to stop the program
     */
    public ChangeDate(final ViewWorldImpl world, final SpecificWorld specific, final ViewClock clockPanel, final AddElemValue<Boolean> stop) {
        super();
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setAlignmentY(BOTTOM_ALIGNMENT);
        this.add(changeLanguage(specific, clockPanel));
        this.add(changeFilter(world, stop));
    }

    @Override
    public final JPanel getElem() {
        return this;
    }

    private JPanel changeFilter(final ViewWorldImpl world, final AddElemValue<Boolean> stop) {
        final JPanel panel = new JPanel(new GridLayout(Filters.values().length + 1, 1));
        panel.setBorder(BorderFactory.createLoweredBevelBorder());
        panel.add(changeFiltDesc);
        panel.setMaximumSize(DimensionComponent.CHANGED_FILTER.getDimension());
        Arrays.asList(Filters.values()).forEach(filter -> {
            final JButton jb = new JButton(Language.getkeyofbundle(filter.getName()));
            panel.add(jb);
            filters.put(jb, filter);
            jb.addActionListener(a -> {
                world.updateFilter(filters.get(a.getSource()).getName());
                if (!stop.getValue()) {
                    world.updateStatus();
                }
            });
        });
        return panel;
    }

    private void updateNameFilter() {
        filters.forEach((jb, f) -> {
            jb.setText(Language.getkeyofbundle(f.getName()));
        });
    }

    private JPanel changeLanguage(final SpecificWorld specific, final ViewClock clockPanel) {
        final AddElemValue<Languages> buttonLanguage = new ButtonLanguage();
        final JPanel panel = new JPanel();
        panel.setPreferredSize(DimensionComponent.CHANGED_LANGUAGE.getDimension());
        panel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createLoweredBevelBorder());
        final JPanel btpanel = (JPanel) buttonLanguage.getElem();
        btpanel.setLayout(new BoxLayout(btpanel, BoxLayout.Y_AXIS));
        changeLangDesc.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(changeLangDesc);
        panel.add(btpanel);
        panel.add(selectButtons(buttonLanguage, specific, clockPanel));
        return panel;
    }

    private JButton selectButtons(final AddElemValue<Languages> buttonLanguage, final SpecificWorld specific, final ViewClock clockPanel) {
        final LanguageSet l = new Language();
        jb.setAlignmentX(Component.CENTER_ALIGNMENT);
        jb.addActionListener(a -> {
            l.setcurrentbundle(buttonLanguage.getValue());
            clockPanel.updateLanguageElem();
            specific.updateLanguageElem();
            changeLangDesc.setText(Language.getkeyofbundle("ChangeL"));
            changeFiltDesc.setText(Language.getkeyofbundle("ChangeF"));
            updateNameFilter();
            jb.setText(Language.getkeyofbundle("Confirm"));
        });
        return jb;
    }

}
