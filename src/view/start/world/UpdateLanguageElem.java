package view.start.world;

/**
 * Interface to update the interface elements after changing the language.
 *
 */
public interface UpdateLanguageElem {

    /**
     * Method that resets all the strings of the class it implements by setting them with the new chosen language.
     */
    void updateLanguageElem();

}
