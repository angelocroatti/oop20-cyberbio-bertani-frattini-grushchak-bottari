package view.menu.data.setting;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import model.properties.defaultdata.DefaultDataContainer;

/**
 * Class that manages one panel per sect the dimensions inherent to the world thanks to four buttons 
 * that increase and decrease the value.
 *
 */
public class AddWorldSize extends JPanel implements AddElemValue<Integer> {

    private static final long serialVersionUID = -4009059151616472243L;
    private static final int LITTLE_RANGE = 1;
    private static final int BIG_RANGE = 10;
    private Integer value;
    private final DefaultDataContainer<Integer> data;
    private final JButton dec = new JButton("<");
    private final JButton ddec = new JButton("<<");
    private final JButton inc = new JButton(">");
    private final JButton dinc = new JButton(">>");

    /**
     * Constructor that implements the panel with passed title and with the keys to increase and decrease the value 
     * within the range passed for parameter.
     * @param description title
     * @param defaultDataContainer range of permissible and default values
     */
    public AddWorldSize(final String description, final DefaultDataContainer<Integer> defaultDataContainer) {
        super();
        this.value = defaultDataContainer.getDafaultValue().intValue();
        this.data = defaultDataContainer;
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        final JLabel title = new JLabel(description);
        title.setAlignmentX(CENTER_ALIGNMENT);
        this.add(title);
        this.add(display());
        this.add(new JSeparator());
    }

    @Override
    public final Integer getValue() {
        return value;
    }

    @Override
    public final JPanel getElem() {
        return this;
    }

    private JPanel display() {
        final JPanel panel = new JPanel();
        final JLabel text = new JLabel(value.toString());
        dec.addActionListener(a -> changeValue(text, -LITTLE_RANGE));
        ddec.addActionListener(a -> changeValue(text, -BIG_RANGE));
        inc.addActionListener(a -> changeValue(text, +LITTLE_RANGE));
        dinc.addActionListener(a -> changeValue(text, +BIG_RANGE));
        panel.add(ddec);
        panel.add(dec);
        panel.add(text);
        panel.add(inc);
        panel.add(dinc);
        return panel;
    }

    private void changeValue(final JLabel text, final int v) {
        value = value + v;
        text.setText(value.toString());

        inc.setEnabled(limitControll(LITTLE_RANGE, data.getMaximumValue().intValue()));
        dinc.setEnabled(limitControll(BIG_RANGE, data.getMaximumValue().intValue()));
        dec.setEnabled(limitControll(LITTLE_RANGE, data.getMinimumValue().intValue()));
        ddec.setEnabled(limitControll(BIG_RANGE, data.getMinimumValue().intValue()));

    }

    private boolean limitControll(final int n, final int lim) {
        if (lim == data.getMaximumValue().intValue()) {
            return value + n <= lim;
        } else {
            return value - n >= lim;
        }
    }

}
