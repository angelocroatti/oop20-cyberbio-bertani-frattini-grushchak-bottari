package view.menu.data.setting;

import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import controller.SimulationInitializator;
import controller.StartingMode;
import controller.file.data.input.FileSet;
import controller.file.data.input.FileSetImpl;
import model.color.filter.Filters;
import model.language.Language;
import model.properties.defaultdata.CellsDefaultUtils;
import model.properties.defaultdata.GenesDefaultUtils;
import model.properties.defaultdata.WorldDefaultUtils;
import view.menu.language.MenuFrame;

/**
 * Class that manages a frame containing all the panels to set the values by the user.
 * Once you press the start button, call the FileSet class and upload the set data to file.
 *
 */
public class DataMenuImpl extends JFrame implements MenuFrame {

    private static final long serialVersionUID = -4256558330444585208L;

    private final AddElem menuDesc = new AddMenuDescriptrion();
    private final AddElemValue<StartingMode> modProgram = new AddStartingMode(Language.getkeyofbundle("Modality"));
    private final AddElemValue<Integer> worldHSize = new AddWorldSize(Language.getkeyofbundle("SizeHWorld"), WorldDefaultUtils.WORLD_HEIGHT);
    private final AddElemValue<Integer> worldWSize = new AddWorldSize(Language.getkeyofbundle("SizeWWorld"), WorldDefaultUtils.WORLD_WIDTH);
    private final AddElemValue<Integer> upDateview = new AddUpDateView();
    private final AddElemValue<Float> colorChoose = new AddColorChoose();
    private final AddElemValue<Integer> filterColor = new AddFilterColor(colorChoose);

    private final AddElemValue<Integer> maxEnergy = new AddSlider(Language.getkeyofbundle("MaxEnergy"), CellsDefaultUtils.MAX_CELL_ENERGY.getMinimumValue(), 
            CellsDefaultUtils.MAX_CELL_ENERGY.getMaximumValue(), CellsDefaultUtils.MAX_CELL_ENERGY.getDafaultValue());
    private final AddElemValue<Integer> maxSunLight = new AddSlider(Language.getkeyofbundle("MaxSunLight"), GenesDefaultUtils.SUN_ENERGY.getMinimumValue(),
            GenesDefaultUtils.SUN_ENERGY.getMaximumValue(), GenesDefaultUtils.SUN_ENERGY.getDafaultValue());
    private final AddElemValue<Integer> sunPenetration = new AddSlider(Language.getkeyofbundle("SunPenetration"), GenesDefaultUtils.SUN_PENETRATION.getMinimumValue(),
            GenesDefaultUtils.SUN_PENETRATION.getMaximumValue(), GenesDefaultUtils.SUN_PENETRATION.getDafaultValue());
    private final AddElemValue<Integer> mineralDepth = new AddSlider(Language.getkeyofbundle("MineralDepth"), GenesDefaultUtils.MINERALS_DEPTH.getMinimumValue(),
            GenesDefaultUtils.MINERALS_DEPTH.getMaximumValue(), GenesDefaultUtils.MINERALS_DEPTH.getDafaultValue());
    private final AddElemValue<Integer> sizeGenoma = new AddSlider(Language.getkeyofbundle("GenomSize"), CellsDefaultUtils.GENOME_SIZE.getMinimumValue(),
            CellsDefaultUtils.GENOME_SIZE.getMaximumValue(), CellsDefaultUtils.GENOME_SIZE.getDafaultValue());
    private final AddElemValue<Integer> maxAge = new AddSlider(Language.getkeyofbundle("MaxAge"), CellsDefaultUtils.MAX_AGE.getMinimumValue(),
            CellsDefaultUtils.MAX_AGE.getMaximumValue(), CellsDefaultUtils.MAX_AGE.getDafaultValue());
 
    /**
     * Constructor that adds all the elements to the window and sets its basic features.
     */
    public DataMenuImpl() {
        super();
        this.setIconImage(Icon.SETTING.getIcon().getImage());
        this.setTitle(Language.getkeyofbundle("TitleMenu"));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.addElem();
        this.pack();
        this.setLocationRelativeTo(null);
    }

    @Override
    public final void showMenu() {
        this.setVisible(true);
    }

    private void setValue() {
        final FileSet f = new FileSetImpl();
        final int filterValue = filterColor.getValue();

        if (filterValue != Filters.NUTRITION.getValue()) {
            f.addtoFile(maxEnergy.getValue(), maxSunLight.getValue(), sizeGenoma.getValue(), maxAge.getValue(),
                    worldHSize.getValue(), worldWSize.getValue(), upDateview.getValue(), filterValue, modProgram.getValue(),
                    (float) sunPenetration.getValue() / 100, (float) mineralDepth.getValue() / 100, colorChoose.getValue());
        } else {
            f.addtoFile(maxEnergy.getValue(), maxSunLight.getValue(), sizeGenoma.getValue(), maxAge.getValue(),
                    worldHSize.getValue(), worldWSize.getValue(), upDateview.getValue(), filterValue, modProgram.getValue(),
                    (float) sunPenetration.getValue() / 100, (float) mineralDepth.getValue() / 100);
        }
    }

    private JPanel addElem() {
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        this.getContentPane().add(BorderLayout.CENTER, panel);

        panel.add(menuDesc.getElem());
        panel.add(modProgram.getElem());
        panel.add(maxEnergy.getElem());
        panel.add(maxSunLight.getElem());
        panel.add(sunPenetration.getElem());
        panel.add(mineralDepth.getElem());
        panel.add(sizeGenoma.getElem());
        panel.add(maxAge.getElem());
        panel.add(worldHSize.getElem());
        panel.add(worldWSize.getElem());
        panel.add(upDateview.getElem());
        panel.add(filterColor.getElem());
        panel.add(new JSeparator());
        panel.add(startButton()); 
        return panel;
    }


    private JPanel startButton() {
        final JPanel panel = new JPanel();
        final JButton jb = new JButton(Language.getkeyofbundle("ButtonStart"));
        jb.addActionListener(a -> {
            setValue();
            this.dispose();
            new SimulationInitializator();
        });
        panel.add(jb);
        return panel;
    }
}
