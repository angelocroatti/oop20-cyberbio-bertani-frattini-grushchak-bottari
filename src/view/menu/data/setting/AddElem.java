package view.menu.data.setting;

import java.awt.Component;

/**
 * Interface that will be implemented by classes to add graphics elements that extend Component.
 * 
 */
public interface AddElem {

    /**
     * Method that implements a Component type graphic element.
     * @return the graphical element implemented in the class
     */
    Component getElem();

}
