package view.menu.data.setting;

import model.language.Language;
import model.language.Languages;

/**
 * Class that tests the setting of the color tones of the filters.
 *
 */
public final class TestColorChoose {

    private TestColorChoose() { };

    /**
     * Launch AddColorChoose with default language.
     * @param args default
     * @throws java.io.IOException default
     */
    public static void main(final String[] args) throws java.io.IOException {
        new Language().setcurrentbundle(Languages.ITALIAN);
        final AddColorChoose c = new AddColorChoose();
        c.getElem();
    }

}
