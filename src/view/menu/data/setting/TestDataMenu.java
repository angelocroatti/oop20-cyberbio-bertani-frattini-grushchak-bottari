package view.menu.data.setting;

import model.language.Language;
import model.language.Languages;
import view.menu.language.MenuFrame;

/**
 * Test class that launches the graphic interface for setting the simulation values with default language setting.
 *
 */
public final class TestDataMenu {

    private TestDataMenu() { };

    /**
     * Launch DataMenu with default language.
     * @param args default
     */
    public static void main(final String[] args) {
        new Language().setcurrentbundle(Languages.ITALIAN);
        final MenuFrame d = new DataMenuImpl();
        d.showMenu();
    }

}
