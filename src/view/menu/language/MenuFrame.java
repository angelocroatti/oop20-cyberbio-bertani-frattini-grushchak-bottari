package view.menu.language;

/**
 * Interface that will be implemented by the classes that will implement menus.
 *
 */
public interface MenuFrame {

    /**
     * Method that shows the menu window.
     */
    void showMenu();

}
