package view.menu.language;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;

import model.language.Languages;
import view.menu.data.setting.AddElemValue;

/**
 * Class that manages a panel containing a button for each implemented language.
 *
 */
public class ButtonLanguage extends JPanel implements AddElemValue<Languages> {

    private static final long serialVersionUID = 7435970164445667330L;
    private final Map<JRadioButtonMenuItem, Languages> buttons = new HashMap<>();

    public ButtonLanguage() {
        super();
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        Arrays.asList(Languages.values()).forEach(l -> {
            final JRadioButtonMenuItem jb = new JRadioButtonMenuItem();
            this.setAlignmentX(CENTER_ALIGNMENT);
            jb.setIcon(l.getIcon());
            if (l.equals(Languages.ENGLISH)) {
                jb.setSelected(true);
            }
            jb.addActionListener(a -> {
                buttons.forEach((but, val) -> but.setSelected(false));
                jb.setSelected(true);
            });
            buttons.put(jb, l);
            this.add(jb);
        });
    }

    @Override
    public final JPanel getElem() {
        return this;
    }

    @Override
    public final Languages getValue() {
        final JRadioButtonMenuItem select = buttons.keySet().stream().filter(jb -> jb.isSelected()).findFirst().get();
        return buttons.get(select);
    }

}
