package view.menu.language;

/**
 * Test class that launches the graphical language setting interface.
 *
 */
public final class TestLanguageMenu {

    private TestLanguageMenu() { };

    /**
     * Launch LanguageMenu.
     * @param args default
     */
    public static void main(final String[] args) {
        final MenuFrame firstMenu = new LanguageMenuImpl();
        firstMenu.showMenu();
    }
}
