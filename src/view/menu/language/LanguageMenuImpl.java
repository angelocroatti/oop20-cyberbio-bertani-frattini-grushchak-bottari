package view.menu.language;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.language.Language;
import model.language.LanguageSet;
import model.language.Languages;
import view.menu.data.setting.AddElemValue;
import view.menu.data.setting.DataMenuImpl;
import view.menu.data.setting.DimensionComponent;
import view.menu.data.setting.Icon;

/**
 * Class that manages a window with the logo and the possibility to select the language.
 *
 */
public class LanguageMenuImpl extends JFrame implements MenuFrame {

    private static final long serialVersionUID = -4256558330444585208L;
    private final AddElemValue<Languages> buttons = new ButtonLanguage();

    /**
     * Constructor that implements a panel with the logo and as many radio buttons as there are languages that can be selected 
     * with their icons.
     */
    public LanguageMenuImpl() {
        super();
        this.getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.LINE_AXIS));
        this.add(addLogo());
        this.add(addLanguage());
        this.setIconImage(Icon.LOGO.getIcon().getImage());
        this.setTitle("SELECT LANGUAGE");
        this.pack();
        this.setLocationRelativeTo(null);

    }

    @Override
    public final void showMenu() {
        this.setVisible(true);
    }

    private JPanel addLanguage() {
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(buttons.getElem());
        panel.add(addButtonStart());
        return panel;
    }

    private JPanel addLogo() {
        final JPanel panel = new JPanel();
        panel.setSize(DimensionComponent.LOGO_IMAGE.getDimension());
        panel.add(new JLabel(new ImageIcon(Icon.LOGO.getIcon().getImage())));
        return panel;
    }

    private JPanel addButtonStart() {
        final JPanel panel = new JPanel();
        final JButton start = new JButton("START");
        start.addActionListener(a -> {
            final LanguageSet l = new Language();
            l.setcurrentbundle(buttons.getValue());
            this.dispose();
            final MenuFrame secondMenu = new DataMenuImpl();
            secondMenu.showMenu();
        });
        panel.add(start);
        return panel;
    }

}

