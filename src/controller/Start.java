package controller;

import controller.clock.ControllerImpl;
import model.Model;
import model.ModelImpl;
import model.world.World;
import model.world.WorldImp;
import view.menu.language.MenuFrame;
import view.start.world.ApplicationFrame;
import view.start.world.ApplicationFrameImpl;

/**
 * Class that launches the application.
 *
 */
public class Start implements Runnable {

    private final World world;
    private Model t;

    /**
     * Builder who loses the world as a parameter and launches the graphics and iteration of the world.
     * @param world the world to view
     */
    public Start(final World world) {
       super();
       this.world = world;
       t = new ModelImpl((WorldImp) world);
    }

    @Override
    public final void run() {
        final MenuFrame f = new ApplicationFrameImpl(world);
        f.showMenu();
        new ControllerImpl((ApplicationFrame) f).start(t);
    }
}
