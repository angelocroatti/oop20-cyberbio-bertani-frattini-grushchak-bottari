package controller;

/**
 * 
 * Different modes of initialization of world.
 *
 */
public enum StartingMode {
    /**
     * The mode with only one cell with only photosynthesis genes.
     */
    SINGLE_PHOTOSYNTHESIS_CELL,
    /**
     * The mode with a lot of cells with random generated genomes.
     */
    RANDOM_GENOME_CELLS;
}
