package controller.file.data.input;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import controller.StartingMode;
import model.color.filter.Filters;

/**
 * Classes that manage the loading of user-set from specific files.
 *
 */
public class FileGetSettingImpl implements FileGetSetting {

    private int maxEnergy;
    private int maxSunLight;
    private int sizeGenoma;
    private int maxAge;
    private int heightWorld;
    private int widthWorld;
    private int upDateFrame;
    private int colorFilter;
    private int mod;
    private float sunPenetration;
    private float mineralDepth;
    private float hue;

    /**
     * Builder that uploads user-set data from file.
     *
     */
    public FileGetSettingImpl() {
        final FileSet f = new FileSetImpl();
        try (DataInputStream dstream = new DataInputStream(new BufferedInputStream(new FileInputStream(f.getFile())));) {
            maxEnergy = dstream.readInt();
            maxSunLight = dstream.readInt();
            sizeGenoma = dstream.readInt();
            maxAge = dstream.readInt();
            heightWorld = dstream.readInt();
            widthWorld = dstream.readInt();
            upDateFrame = dstream.readInt();
            colorFilter = dstream.readInt();
            mod = dstream.readInt();
            sunPenetration = dstream.readFloat();
            mineralDepth = dstream.readFloat();
            hue = dstream.readFloat();
            f.addtoFile(maxEnergy, maxSunLight, sizeGenoma, maxAge, heightWorld, widthWorld, upDateFrame, colorFilter, getMod(),
                    sunPenetration, mineralDepth, hue);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final StartingMode getMod() {
        switch (mod) {
            case 0: return StartingMode.RANDOM_GENOME_CELLS;
            case 1: return StartingMode.SINGLE_PHOTOSYNTHESIS_CELL;
            default: throw new IllegalArgumentException("STARTING MODE NON ESISTENTE");
        }
    }

    @Override
    public final int getMaxEnery() {
        return maxEnergy;
    }

    @Override
    public final int getSunLight() {
        return maxSunLight;
    }

    @Override
    public final int getHeightWorld() {
        return heightWorld;
    }

    @Override
    public final int getWidthWorld() {
        return widthWorld;
    }

    @Override
    public final int getUpDateView() {
        return upDateFrame;
    }

    @Override
    public final Filters getColorFilter() {
        return Filters.getEnum(colorFilter);
    }

    @Override
    public final float getHue() {
        return hue;
    }

    @Override
    public final int getSizeGenoma() {
        return sizeGenoma;
    }

    @Override
    public final int getMaxAge() {
        return maxAge;
    }

    @Override
    public final float getSunPenetration() {
        return sunPenetration;
    }

    @Override
    public final float getMineralDepth() {
        return mineralDepth;
    }

}
