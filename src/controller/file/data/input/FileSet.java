package controller.file.data.input;

import java.io.File;

import controller.StartingMode;

/**
 * Interface that will be implemented classes that uploads data passed to file by argument.
 *
 */
public interface FileSet {

    /**
     * Method that takes all the parameters passed by topic and writes them to a certain file.
     * @param maxEnergy maxEnergy energy max of a cell
     * @param maxSunLight maxSunLight energy of Sun max
     * @param sizeGenoma genome length
     * @param maxAge maximum cell age
     * @param worldHSize heightWorld height of World 
     * @param worldWSize widthWorld width of World
     * @param upDateview number of frames after which the graphical user interface must be updated
     * @param filterValue type of color filter
     * @param mod program execution modalities
     * @param sunPenetration sun penetration percentage
     * @param mineralDepth percentage depth of minerals
     * @param hue shade used by color filters
     * 
     */
     void addtoFile(int maxEnergy, int maxSunLight, int sizeGenoma, int maxAge, int worldHSize, int worldWSize, int upDateview,
             int filterValue, StartingMode mod, float sunPenetration, float mineralDepth, float hue);

    /**
     * Method that takes all the parameters passed by topic and writes them to a given file with the default color shade.
     * @param maxEnergy maxEnergy energy max of a cell
     * @param maxSunLight maxSunLight energy of Sun max
     * @param sizeGenoma genome length
     * @param maxAge maximum cell age
     * @param worldHSize heightWorld height of World 
     * @param worldWSize widthWorld width of World
     * @param upDateview number of frames after which the graphical user interface must be updated
     * @param filterValue type of color filter
     * @param mod program execution modalities
     * @param sunPenetration sun penetration percentage
     * @param mineralDepth percentage depth of minerals
     * 
     */
     void addtoFile(int maxEnergy, int maxSunLight, int sizeGenoma, int maxAge, int worldHSize, int worldWSize, int upDateview,
             int filterValue, StartingMode mod, float sunPenetration, float mineralDepth);

     /**
      * Method that returns the file to which you want to save the data.
      * @return file
      */
     File getFile();
}
