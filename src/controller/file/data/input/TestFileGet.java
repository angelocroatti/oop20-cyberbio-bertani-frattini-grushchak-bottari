package controller.file.data.input;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import controller.StartingMode;
import model.color.filter.Filters;
import model.properties.defaultdata.ViewDefaultUtils;

/**
 * Test to verify the correctness of the bundle translation.
 * 
 */
class TestFileGet {

    private final FileSet setterTest = new FileSetImpl();
    private FileGetSetting getterTest;

    /**
     * GENERAL TEST.
     */
    @Test
    public void testGeneral() {
        setterTest.addtoFile(10, 16, 32, 64, 90, 100, 180, 2, StartingMode.RANDOM_GENOME_CELLS, (float) 0.5, (float) 0, (float) 1);

        getterTest = new FileGetSettingImpl();

        assertEquals(10, getterTest.getMaxEnery(), "The value should be 10 not " + getterTest.getMaxEnery());
        assertEquals(16, getterTest.getSunLight(), "The value should be 16 not " + getterTest.getSunLight());
        assertEquals(32, getterTest.getSizeGenoma(), "The value should be 32 not " + getterTest.getSizeGenoma());
        assertEquals(64, getterTest.getMaxAge(), "The value should be 64 not " + getterTest.getMaxAge());
        assertEquals(90, getterTest.getHeightWorld(), "The value should be 90 not " + getterTest.getHeightWorld());
        assertEquals(100, getterTest.getWidthWorld(), "The value should be 100 not " + getterTest.getWidthWorld());
        assertEquals(180, getterTest.getUpDateView(), "The value should be 180 not " + getterTest.getUpDateView());
        assertEquals(Filters.NUTRITION, getterTest.getColorFilter(), "The value should be NUTRITION not " + getterTest.getColorFilter());
        assertEquals(StartingMode.RANDOM_GENOME_CELLS, getterTest.getMod(), "The value should be RANDOM_GENOME_CELLS not " + getterTest.getMod());
        assertEquals((float) 0.5, getterTest.getSunPenetration(), "The value should be 0.5 not " + getterTest.getSunPenetration());
        assertEquals((float) 0, getterTest.getMineralDepth(), "The value should be 0.0 not " + getterTest.getMineralDepth());
        assertEquals((float) 1, getterTest.getHue(), "The value should be 1.0 not " + getterTest.getHue());
    }

    /**
     * WHITOUT HUE TEST.
     */
    @Test
    public void testWhitoutHue() {
        final float sp = (float) 0.6;
        final float md = (float) 0.4;

        setterTest.addtoFile(180, 100, 90, 64, 32, 16, 10, 0, StartingMode.SINGLE_PHOTOSYNTHESIS_CELL, sp, md);

        getterTest = new FileGetSettingImpl();

        assertEquals(180, getterTest.getMaxEnery(), "The value should be 180 not " + getterTest.getMaxEnery());
        assertEquals(100, getterTest.getSunLight(), "The value should be 100 not " + getterTest.getSunLight());
        assertEquals(90, getterTest.getSizeGenoma(), "The value should be 90 not " + getterTest.getSizeGenoma());
        assertEquals(64, getterTest.getMaxAge(), "The value should be 64 not " + getterTest.getMaxAge());
        assertEquals(32, getterTest.getHeightWorld(), "The value should be 32 not " + getterTest.getHeightWorld());
        assertEquals(16, getterTest.getWidthWorld(), "The value should be 16 not " + getterTest.getWidthWorld());
        assertEquals(10, getterTest.getUpDateView(), "The value should be 10 not " + getterTest.getUpDateView());
        assertEquals(Filters.AGE, getterTest.getColorFilter(), "The value should be AGE not " + getterTest.getColorFilter());
        assertEquals(StartingMode.SINGLE_PHOTOSYNTHESIS_CELL, getterTest.getMod(), "The value should be SINGLE_PHOTOSYNTHESIS_CELL not " + getterTest.getMod());
        assertEquals(sp, getterTest.getSunPenetration(), "The value should be 0.6 not " + getterTest.getSunPenetration());
        assertEquals(md, getterTest.getMineralDepth(), "The value should be 0.4 not " + getterTest.getMineralDepth());
        assertEquals(ViewDefaultUtils.COLOR_HSB_RANGE.getDafaultValue(), getterTest.getHue(), "The value should be 0.0 not " + getterTest.getHue());
    }

    /**
     * EXEPTION TYPE FILTER TEST.
     */
    @Test
    public void testExeptionFilter() {
        final float sp = (float) 0.86;
        final float md = (float) 0.31;
        final float h = (float) 0.7;

        setterTest.addtoFile(3 * 16, 3 * 1, 3 * 2, 3 * 3, 3 * 4, 3 * 5, 3 * 8, 5, StartingMode.SINGLE_PHOTOSYNTHESIS_CELL, sp, md, h);

        final FileGetSetting getterExepTest = new FileGetSettingImpl();
        assertEquals(3 * 16, getterExepTest.getMaxEnery(), "The value should be 48 not " + getterExepTest.getMaxEnery());
        assertEquals(3 * 1, getterExepTest.getSunLight(), "The value should be 3 not " + getterExepTest.getSunLight());
        assertEquals(3 * 2, getterExepTest.getSizeGenoma(), "The value should be 6 not " + getterExepTest.getSizeGenoma());
        assertEquals(3 * 3, getterExepTest.getMaxAge(), "The value should be 9 not " + getterExepTest.getMaxAge());
        assertEquals(3 * 4, getterExepTest.getHeightWorld(), "The value should be 12 not " + getterExepTest.getHeightWorld());
        assertEquals(3 * 5, getterExepTest.getWidthWorld(), "The value should be 15 not " + getterExepTest.getWidthWorld());
        assertEquals(3 * 8, getterExepTest.getUpDateView(), "The value should be 24 not " + getterExepTest.getUpDateView());
        assertThrows(IllegalArgumentException.class, () -> getterExepTest.getColorFilter(), "There should be no associated value");
        assertEquals(StartingMode.SINGLE_PHOTOSYNTHESIS_CELL, getterExepTest.getMod(), "The value should be SINGLE_PHOTOSYNTHESIS_CELL not " + getterExepTest.getMod());
        assertEquals(sp, getterExepTest.getSunPenetration(), "The value should be 0.86 not " + getterExepTest.getSunPenetration());
        assertEquals(md, getterExepTest.getMineralDepth(), "The value should be 0.31 not " + getterExepTest.getMineralDepth());
        assertEquals(h, getterExepTest.getHue(), "The value should be 0.7 not " + getterExepTest.getHue());
    }
}
